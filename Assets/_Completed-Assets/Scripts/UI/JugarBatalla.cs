using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Complete{
    public class JugarBatalla : MonoBehaviour{

        public GameObject boton2, boton3, boton4, botonSalir;
        public Text textTitulo, textMensaje;
        public GameObject gameManager;

        public bool pausar = true;

        public void PlayGame(int numeroJugadores){
            if (pausar){
                Time.timeScale = 1;
                pausar = false;
                gameManager.GetComponent<GameManager>().jugarBatalla = true;
                gameManager.GetComponent<GameManager>().IniciarJuego(numeroJugadores);
                OcultarMenu();
            } else {
                Time.timeScale = 0;
                pausar = true;
                VerMenu();
            }
        }

        public void VerMenu(){
            boton2.gameObject.SetActive(true);
            boton3.gameObject.SetActive(true);
            boton4.gameObject.SetActive(true);
            botonSalir.gameObject.SetActive(true);
            textMensaje.text = "selecciona el número de jugadores";
            textTitulo.text = "TANKS!";
        }

        public void OcultarMenu(){
            boton2.gameObject.SetActive(false);
            boton3.gameObject.SetActive(false);
            boton4.gameObject.SetActive(false);
            botonSalir.gameObject.SetActive(false);
            textMensaje.text = "";
            textTitulo.text = "";
        }
        void Start(){
            pausar = true;
        }

        public void Salir(){
            Application.Quit();
        }
    }
}

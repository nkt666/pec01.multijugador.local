**Proyecto Multijugador Local - Tanks**

[Documentación del proyecto](https://gitlab.com/nkt666/pec01.multijugador.local/-/blob/master/Docs/PEC-01.multijugador.local.pdf)

[Builds del Proyecto .zip](https://drive.google.com/file/d/1gOJ-s_g3XkWscFAM1giVtVgveLo1uZ0S/view?usp=sharing)

En este proyecto se ha implementado los requerimientos de un videojuego Multijugador local.


**pantalla de inicio de selección de jugadores**

![intro](/Docs/00.intro.png?raw=true  "intro")


**Las pantallas se distribuyen generandose una nueva para cada nuevo jugador cuando se presiona la tecla space**

![pantallas](/Docs/01.pantallas.png?raw=true "pantallas")


**pantalla de fin de juego y reinicio de partida.**

![fin](/Docs/02.fin.juego.png?raw=true "fin")


by [nkt]
